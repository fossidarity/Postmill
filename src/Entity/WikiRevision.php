<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WikiRevisionRepository")
 * @ORM\Table(name="wiki_revisions")
 * @ApiResource(
 * 	attributes={
 * 		"normalization_context"={"groups"={"abbreviated_relations", "wiki_revision:read"}},
 * 	},
 *  collectionOperations={"get"},
 *  itemOperations={"get"}
 * )
 */
class WikiRevision {
    /**
     * @ORM\Column(type="uuid")
     * @ORM\Id()
     *
     * @Groups({"abbreviated_relations"})
     *
     * @var Uuid
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     *
     * @Groups({"wiki_revision:read"})
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     *
     * @Groups({"wiki_revision:read"})
     *
     * @var string
     */
    private $body;

    /**
     * @ORM\JoinColumn(nullable=false)
     * @ORM\ManyToOne(targetEntity="WikiPage", inversedBy="revisions")
     *
     * @var WikiPage
     */
    private $page;

    /**
     * @ORM\Column(type="datetimetz")
     * @Groups({"wiki_revision:read"})
     *
     * @var \DateTime
     */
    private $timestamp;

    /**
     * @ORM\JoinColumn(nullable=false)
     * @ORM\ManyToOne(targetEntity="User")
     *
     * @Groups({"wiki_revision:read"})
     *
     * @var User
     */
    private $user;

    public function __construct(
        WikiPage $page,
        string $title,
        string $body,
        User $user,
        \DateTime $timestamp = null
    ) {
        $this->id = Uuid::uuid4();
        $this->page = $page;
        $this->title = $title;
        $this->body = $body;
        $this->user = $user;
        $this->timestamp = $timestamp ?:
            \DateTime::createFromFormat('U.u', microtime(true));

        $this->page->addRevision($this);
    }

    public function getId(): Uuid {
        return $this->id;
    }

    public function getTitle(): string {
        return $this->title;
    }

    public function getBody(): string {
        return $this->body;
    }

    public function getPage(): WikiPage {
        return $this->page;
    }

    public function getTimestamp(): \DateTime {
        return $this->timestamp;
    }

    public function getUser(): User {
        return $this->user;
    }
}
