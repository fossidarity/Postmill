<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IpBanRepository")
 * @ORM\Table(name="bans")
 * @ApiResource(
 * 	attributes={
 * 		"normalization_context"={"groups"={"abbreviated_relations", "user:read"}},
 * 	},
 *  collectionOperations={},
 *  itemOperations={
 *      "get"={
 *          "method"="GET",
 *          "access_control"="is_granted('ROLE_ADMIN')",
 *      },
 *  },
 *  subresourceOperations={
 *      "api_users_ip_bans_get_subresource"={
 *          "method"="GET",
 *          "access_control"="is_granted('ROLE_ADMIN')",
 *      }
 *  }
 * )
 */
class IpBan {
    /**
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     * @Groups({"abbreviated_relations"})
     *
     * @var int|null
     */
    private $id;

    /**
     * @ORM\Column(type="inet")
     * @Groups({"ip_ban:read"})
     *
     * @var string
     */
    private $ip;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"ip_ban:read"})
     *
     * @var string
     */
    private $reason;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="ipBans")
     * @Groups({"ip_ban:read"})
     *
     * @var User|null
     */
    private $user;

    /**
     * @ORM\JoinColumn(nullable=false)
     * @ORM\ManyToOne(targetEntity="User")
     * @Groups({"ip_ban:read"})
     *
     * @var User
     */
    private $bannedBy;

    /**
     * @ORM\Column(type="datetimetz")
     * @Groups({"ip_ban:read"})
     *
     * @var \DateTime
     */
    private $timestamp;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     * @Groups({"ip_ban:read"})
     *
     * @var \DateTime
     */
    private $expiryDate;

    public function __construct(
        string $ip,
        string $reason,
        ?User $user,
        User $bannedBy,
        \DateTime $expiryDate = null,
        \DateTime $timestamp = null
    ) {
        $this->ip = $ip;
        $this->reason = $reason;
        $this->user = $user;
        $this->bannedBy = $bannedBy;
        $this->expiryDate = $expiryDate;
        $this->timestamp = $timestamp ?: new \DateTime();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getIp(): string {
        return $this->ip;
    }

    public function getReason(): string {
        return $this->reason;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function getBannedBy(): User {
        return $this->bannedBy;
    }

    public function getTimestamp(): \DateTime {
        return $this->timestamp;
    }

    public function getExpiryDate(): ?\DateTime {
        return $this->expiryDate;
    }
}
