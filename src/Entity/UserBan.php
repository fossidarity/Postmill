<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserBanRepository")
 * @ORM\Table(name="user_bans")
 * @ApiResource(
 *  collectionOperations={},
 *  itemOperations={
 *      "get",
 *  },
 *  subresourceOperations={
 *      "api_users_user_bans_get_subresource"={
 *          "method"="GET",
 *          "access_control"="is_granted('ROLE_ADMIN')",
 * 	    	"normalization_context"={"groups"={"abbreviated_relations", "user_ban_read"}},
 *      }
 *  }
 * )
 */
class UserBan {
    /**
     * @ORM\Column(type="uuid")
     * @ORM\Id()
     * @Groups({"abbreviated_relations"})
     *
     * @var Uuid
     */
    private $id;

    /**
     * @ORM\JoinColumn(nullable=false)
     * @ORM\ManyToOne(targetEntity="User", inversedBy="bans")
     * @Groups({"user_ban_read"})
     *
     * @var User
     */
    private $user;

    /**
     * @ORM\Column(type="text")
     * @Groups({"user_ban_read"})
     *
     * @var string
     */
    private $reason;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"user_ban_read"})
     *
     * @var bool
     */
    private $banned;

    /**
     * @ORM\JoinColumn(nullable=false)
     * @ORM\ManyToOne(targetEntity="User")
     * @Groups({"user_ban_read"})
     *
     * @var User
     */
    private $bannedBy;

    /**
     * @ORM\Column(type="datetimetz")
     * @Groups({"user_ban_read"})
     *
     * @var \DateTime
     */
    private $timestamp;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     * @Groups({"user_ban_read"})
     *
     * @var \DateTime|null
     */
    private $expiresAt;

    public function __construct(
        User $user,
        string $reason,
        bool $banned,
        User $bannedBy,
        \DateTime $expiresAt = null,
        \DateTime $timestamp = null
    ) {
        if (!$banned && $expiresAt) {
            throw new \DomainException('Unbans cannot have expiry times');
        }

        $this->id = Uuid::uuid4();
        $this->user = $user;
        $this->reason = $reason;
        $this->banned = $banned;
        $this->bannedBy = $bannedBy;
        $this->expiresAt = $expiresAt;
        $this->timestamp = $timestamp ?:
            \DateTime::createFromFormat('U.u', microtime(true));
    }

    public function getId(): Uuid {
        return $this->id;
    }

    public function getUser(): User {
        return $this->user;
    }

    public function getReason(): string {
        return $this->reason;
    }

    public function isBan(): bool {
        return $this->banned;
    }

    public function getBannedBy(): User {
        return $this->bannedBy;
    }

    public function getTimestamp(): \DateTime {
        return $this->timestamp;
    }

    public function getExpiresAt(): ?\DateTime {
        return $this->expiresAt;
    }

    public function isExpired(): bool {
        if ($this->expiresAt === null) {
            return false;
        }

        return $this->expiresAt < \DateTime::createFromFormat('U.u', microtime(true));
    }
}
