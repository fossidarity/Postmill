<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="comment_votes", uniqueConstraints={
 *     @ORM\UniqueConstraint(
 *         name="comment_user_vote_idx",
 *         columns={"comment_id", "user_id"}
 *     )
 * })
 * @ORM\AssociationOverrides({
 *     @ORM\AssociationOverride(name="user", inversedBy="commentVotes")
 * })
 * @ApiResource(
 *  collectionOperations={},
 *  itemOperations={
 *      "get"={
 *          "method"="GET",
 *          "access_control"="is_granted('edit_user', object)",
 *      }
 *  },
 *  subresourceOperations={
 *      "api_users_comment_votes_get_subresource"={
 *          "method"="GET",
 *          "access_control"="is_granted('edit_user', object)",
 * 	    	"normalization_context"={"groups"={"abbreviated_relations"}},
 *      }
 *  }
 * )
 */
class CommentVote extends Vote {
    /**
     * @ORM\JoinColumn(name="comment_id", nullable=false)
     * @ORM\ManyToOne(targetEntity="Comment", inversedBy="votes")
     *
     * @Groups({"abbreviated_relations"})
     *
     * @var Comment
     */
    private $comment;

    public function __construct(User $user, ?string $ip, int $choice, Comment $comment) {
        parent::__construct($user, $ip, $choice);

        $this->comment = $comment;
    }

    public function getComment(): Comment {
        return $this->comment;
    }
}
