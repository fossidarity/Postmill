<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="user_blocks", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"blocker_id", "blocked_id"}, name="user_blocks_blocker_blocked_idx")
 * })
 * @ApiResource(
 *  collectionOperations={},
 *  itemOperations={
 *      "get",
 *  },
 *  subresourceOperations={
 *      "api_users_blocks_get_subresource"={
 *          "method"="GET",
 *          "access_control"="is_granted('ROLE_ADMIN')",
 * 	    	"normalization_context"={"groups"={"abbreviated_relations", "user_block_read"}},
 *      }
 *  }
 * )
 */
class UserBlock {
    /**
     * @ORM\Column(type="uuid")
     * @ORM\Id()
     * @Groups({"abbreviated_relations"})
     *
     * @var Uuid
     */
    private $id;

    /**
     * @ORM\JoinColumn(nullable=false)
     * @ORM\ManyToOne(targetEntity="User", inversedBy="blocks")
     * @Groups({"user_block_read"})
     *
     * @var User
     */
    private $blocker;

    /**
     * @ORM\JoinColumn(nullable=false)
     * @ORM\ManyToOne(targetEntity="User")
     * @Groups({"user_block_read"})
     *
     * @var User
     */
    private $blocked;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"user_block_read"})
     *
     * @var string|null
     */
    private $comment;

    /**
     * @ORM\Column(type="datetimetz")
     * @Groups({"user_block_read"})
     *
     * @var \DateTime
     */
    private $timestamp;

    public function __construct(User $blocker, User $blocked, ?string $comment) {
        if ($blocker === $blocked) {
            throw new \InvalidArgumentException();
        }

        $this->id = Uuid::uuid4();
        $this->blocker = $blocker;
        $this->blocked = $blocked;
        $this->comment = $comment;
        $this->timestamp = new \DateTime('@'.time());
    }

    public function getId(): Uuid {
        return $this->id;
    }

    public function getBlocker(): User {
        return $this->blocker;
    }

    public function getBlocked(): User {
        return $this->blocked;
    }

    public function getComment(): ?string {
        return $this->comment;
    }

    public function getTimestamp(): \DateTime {
        return $this->timestamp;
    }
}
