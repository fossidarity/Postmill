<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="submission_votes", uniqueConstraints={
 *     @ORM\UniqueConstraint(
 *         name="submission_user_vote_idx",
 *         columns={"submission_id", "user_id"}
 *     )
 * })
 * @ORM\AssociationOverrides({
 *     @ORM\AssociationOverride(name="user", inversedBy="submissionVotes")
 * })
 * @ApiResource(
 *  collectionOperations={},
 *  itemOperations={
 *      "get"={
 *          "method"="GET",
 *          "access_control"="is_granted('edit_user', object)",
 *      },
 *  },
 *  subresourceOperations={
 *      "api_users_submission_votes_get_subresource"={
 *          "method"="GET",
 *          "access_control"="is_granted('ROLE_ADMIN')",
 * 	    	"normalization_context"={"groups"={"abbreviated_relations"}},
 *      }
 *  }
 * )
 */
class SubmissionVote extends Vote {
    /**
     * @ORM\JoinColumn(name="submission_id", nullable=false)
     * @ORM\ManyToOne(targetEntity="Submission", inversedBy="votes")
     * @Groups({"abbreviated_relations"})
     *
     * @var Submission
     */
    private $submission;

    /**
     * {@inheritdoc}
     */
    public function __construct(User $user, ?string $ip, int $choice, Submission $submission) {
        parent::__construct($user, $ip, $choice);

        $this->submission = $submission;
    }

    public function getSubmission(): Submission {
        return $this->submission;
    }
}
