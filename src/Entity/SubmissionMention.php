<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ApiResource(
 *  collectionOperations={},
 *  itemOperations={
 *      "get",
 *  },
 *  subresourceOperations={
 *      "api_submissions_submission_mentions_get_subresource"={
 *          "method"="GET",
 *          "access_control"="is_granted('access', object)",
 * 	    	"normalization_context"={"groups"={"abbreviated_relations"}},
 *      }
 *  }
 * )
 */
class SubmissionMention extends Notification {
    /**
     * @ORM\ManyToOne(targetEntity="Submission", inversedBy="mentions")
     * @Groups({"abbreviated_relations"})
     *
     * @var Submission
     */
    private $submission;

    public function __construct(User $receiver, Submission $submission) {
        parent::__construct($receiver);

        $this->submission = $submission;
    }

    public function getSubmission(): Submission {
        return $this->submission;
    }

    public function getType(): string {
        return 'submission_mention';
    }
}
